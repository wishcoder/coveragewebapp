$.themes.setDefaults({
	cookieExpiry: 7,
	themeBase: 'http://ajax.googleapos.com/ajax/lib/jqueryui/1.8/themes/',
	icons: '/lib/jqthemes/themes.gif',
	previews: '/lib/jqthemes/themes-preview.gif',
	showPreview: false,
	defaultTheme: 'start'
});

$(function(){
	$('#uiThemes').themes(
		{themes: ['uilightness', 'start']}
	);	
	$('#date').datepicker();
});

/**
*	Application Functions
**/

// Singleton User Session Class
var __userSession = new function(){
	this.sessionId;
	this.user = new User();
	
	this.sessionId = function(_sessionId){
		this.sessionId = _sessionId;
	};
	this.getSessionId = function(){
		return this.sessionId;
	};
	
	this.getUser = function(){
		return this.user;
	}
}

// User Class
function User(){
	this.username;
	this.userid;
	this.type;

	this.setUsername = function(_username){
		this.username = _username;
	};	
	this.getUsername = function(){
		return this.username;
	};
	
	this.setUserid = function(_userid){
		this.userid = _userid;
	};	
	this.getUserid = function(){
		return this.userid;
	};
	
	this.setType = function(_type){
		this.type = _type;
	};	
	this.getType = function(){
		return this.type;
	};
}

// CONSTANTS
const __APP_SERVICE_URL = "/codecoverage/appService";
const __APP_COVERAGE_URL = "/codecoverage/uploadCoverage";

const QUERY_TYPE_VALIDATE_LOGIN = "qtvl";
const QUERY_TYPE_UPDATE_USER = "qtus";
const QUERY_TYPE_GET_PROJECT_LIST = "qtgpl";
const QUERY_TYPE_GET_COVERAGE_LIST = "qtgcl";
const QUERY_TYPE_GET_REPORT_LIST = "qtgrl";

const QUERY_TYPE_SUPER_CREATE_ADMIN_USER = "qtscau";
const QUERY_TYPE_SUPER_GET_ADMIN_USER_LIST = "qtsgaul";

const QUERY_TYPE_ADMIN_GET_SUBORDINATES = "qtags";
const QUERY_TYPE_ADMIN_CREATE_SUBORDINATE = "qtacs";
const QUERY_TYPE_ADMIN_SAVE_PROJECT = "qtasp";
const QUERY_TYPE_ADMIN_SAVE_REPORT = "qtasr";

const USER_TYPE_SUPER_ADMIN = "SUPER";
const USER_TYPE_ADMIN = "ADMIN";
const USER_TYPE_REGULAR = "REGULAR";

// Global arrays
var __dataArray = new Array();
var __projectsArray = new Array();

// Boilerplate code

$(document).ready(function(){
	'use strict';
	$('body').addClass('js');
	$("button, input:submit, input:button").button();
	
	loadLoginView();
});

/**
	loadLoginView
**/
function loadLoginView(){
	$('#contentDiv').load(
	"views/viewLogin.html",
	function(response, status, xhr){
		if(status == "success"){
			$('.login-form span').addClass('checked').children('input').attr('checked', true);
			
			$('.login-form span').on('click', function(){
				if($(this).children('input').attr('checked')){
					$(this).children('input').attr('checked', false);
					$(this).removeClass('checked');
				}else{
					$(this).children('input').attr('checked', true);
					$(this).addClass('checked');
				}
			});
			
			$('#login-form').submit(function(e){
				return false;
			});
			
			$('#btnLogin').click(function(event){
				event.preventDefault();
				validateLogin();
			});
			$('#divView').fadeIn(1000);
			
			$('#username').val($.cookie('wishcoder.coverage.username'));
			$('#username').focus();
		}else{
			var msg = "There is an error: ";
			alert(msg + xhr.status + " " + xhr.statusText);
		}
	});
}

/**
	validateLogin
**/	
function validateLogin(){
	if(!$.trim($('#username').val()).length){
		alert("Warning: Username is required.");
		$('#username').focus();
		return;
	}
	
	if(!$.trim($('#password').val()).length){
		alert("Warning: Password is required.");
		$('#password').focus();
		return;
	}
	
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_VALIDATE_LOGIN,
		sessionid: '',
		username: $.trim($('#username').val()),
		password: $.trim($('#password').val())
	}).done(
		function(responseData){
			data = $(responseData[0]);
			if(data.error != ""){
				alert("Error: " + data.error);
				$('#username').focus();
				return;
			}else{
				__userSession.setSessionId(data.sessionid);
				__userSession.getUser().setUserid(data.userid);
				__userSession.getUser().setUsername(data.username);
				__userSession.getUser().setType(data.usertype);
				
				if($('#chkRemember').is(':checked')){
					$.cookie('wishcoder.coverage.username', $.trim($('#username').val())
					, {expires:365});
				}else{
					$.cookie('wishcoder.coverage.username', "");
				}
				
				$('#divView').fadeOut(1000);
				
				if(__userSession.getUser().getType() === USER_TYPE_SUPER_ADMIN){
					loadSuperView();
				} else if(__userSession.getUser().getType() === USER_TYPE_ADMIN){
					loadAdminView();
				} else if(__userSession.getUser().getType() === USER_TYPE_REGULAR){
					loadUserView();
				}
			}
		}).fail(function(data){
			alert("Error: Unable to process 'validateLogin' request");
			$('#username').focus();
			return;
		}).always(function(data){
			// do nothing
		}
	);
}

/**
	logOut
**/
function logOut(){
	__userSession.setSessionId("");
	__userSession.getUser().setUserid("");
	__userSession.getUser().setUsername("");
	__userSession.getUser().setType("");

	$('#divView').fadeOut(1000);
	
	loadLoginView();
}