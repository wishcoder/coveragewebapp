/**
	loadSuperView
**/
function loadSuperView(){
	$('#contentDev').load(
		"views/viewSuper.html",
		function(response, status, xhr){
			if(status == "success"){
				$('#linkLogout').click(function(event){
					event.preventDefault();
					logOut();
				});
				
				$('#divView').fadeIn(1000);

				$('#superUserForm').find('#btnReset').click(
					function(event){
						resetSuperUserForm();
						event.preventDefault();	
					}
				);
				$('#superUserForm').find('#btnSave').click(
					function(event){
						saveSuperUser('N');
						event.preventDefault();	
					}
				);
				
				loadAdminUsers();	
			}else{
				var msg = "Sorry but there was an error: ";
				alert(msg + xhr.status + " " + xhr.statusText);
			}
		});
}

/**
	loadAdminUsers
**/
function loadAdminUsers(){
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_SUPER_GET_ADMIN_USER_LIST,
		sessionid: __userSession.getSessionId()		
	}).done(
		function(responseData){
			data = $(responseData);
			
			// 1st index is always 'error' object
			if(data[0].hasOwnProperty('error') && data[0].error != ""){
				alert("Error: " + data[0].error);
				return;
			}else{
				updateAdminUsersList(data);
			}
			$('#superUserForm').find('#username').focus();
		}).fail(function(data){
			alert("Error: Unable to process 'loadAdminUsers' request");
			return;
		}).always(function(data){
			// finished
		});
}

/**
	updateAdminUsersList
**/
function updateAdminUsersList(data){
	__dataArray = new Array();
	$.each($(data), function(idx, obj){
		if(idx === 0){
			return; // 1st element is error object
		}
		
		__dataArray[obj.userid] = obj;
		var content = getSuperUserRow(obj);
		$('#tableSuperUserBody').append(content);
	});
	
	initSuperTableClicks();
}

/**
	getSuperUserRow
**/
function getSuperUserRow(obj){
	var content = "<tr><td><a href='#' data-user-id='" + obj.userid + "'>"
	+ obj.username + "</a></td>"
	+ "<td>" + obj.active + "</td>"
	+ "<td align='center'>"; 
	if(obj.active == true){
		content += "<span data-user-id='" + obj.userid + "' class='ui-icon ui-icon-circle-close'></span>";
	}
	content += "</td></tr>"
	return content;
}

/**
	initSuperTableClicks
**/
function initSuperTableClicks(){
	$('#tableSuperUserBody a').click(
		function(event){
			var userid = $(this).data("userid");
			var user = __dataArray[userid];
			if(!user){
				resetAdminUserForm();
			}else{
				$('#superUserForm').find('#username').prop('disabled', true);
				$('#superUserForm').find('#username').val(user.username);
				$('#superUserForm').find('#password').val(user.password);
				$('#superUserForm').find('#userid').val(user.userid);
				$('#superUserForm').find('#saveLabel').html("User [Update]");
				$('#superUserForm').find('#password').focus();
			}	
			event.preventDefault();	
		}
	);
	
	$('#tableSuperUserBody span').click(
		function(event){
			var userid = $(this).data("userid");
			var user = __dataArray[userid];
			
			var answer = confirm("Are you sure you want to delete user '"
			+ user.username + "'?");
			
			if(answer){
				$('#superUserForm').find('#userid').val(user.userid);
				saveSuperUser('Y');
			}else{
				return false;
			}
			event.preventDefault();
		}
	);
}

/**
	saveSuperUser
**/
function saveSuperUser(isDelete){
	if(isDelete === "N"){
		if(!$.trim($('#superUserForm').find('#username').val()).length){
			alert("Warning: Username is required.");
			$('#superUserForm').find('#username').focus();
			return;
		}
		
		if(!$.trim($('#superUserForm').find('#password').val()).length){
			alert("Warning: Password is required.");
			$('#superUserForm').find('#password').focus();
			return;
		}
	}
	
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_SUPER_CREATE_ADMIN_USER,
		sessionid: __userSession.getSessionId(),
		userid: $('#superUserForm').find('#userid').val(),
		username:  (isDelete === "N") ? $.trim($('#superUserForm').find('#username').val()) : "",
		password: (isDelete === "N") ? $.trim($('#superUserForm').find('#password').val()) : ""
	}).done(
		function(responseData){
			data = responseData[0];
			if(data.error != ""){
				alert("Error: " + data.error);
				$('#adminUserForm').find('#username').focus();
				return;
			}else{
				delete __dataArray[data.userid];
				__dataArray[data.userid] = data;
				
				var content = getSuperUserRow(data);
				$('#superTableUser td').filter(
					function(){
						return $(this).text().toLowerCase() == __dataArray[data.userid].username;
					}
				).parent().replaceWith(content);
				
				if(!$.trim($('#superUserForm').find('#userid').val()).length){
					// new entry. add to user table
					var content = getSuperUserRow(data);
					$('#tableSuperUserBody').append(content);
					$('#superUserForm').find('#userid').val(data.userid);
				}	
				
				if(isDelete === "Y"){
					resetSuperUserForm();
				}else{
					$('#superUserForm').find('#username').prop('disabled', true);
					$('#superUserForm').find('#userid').val(user.userid);
					$('#superUserForm').find('#saveLabel').html("User [Update]");
					$('#superUserForm').find('#password').focus();
				}
				
				initSuperTableClicks();
			}
		}).fail(function(data){
			alert("Error: Unable to process 'saveSuperUser' request");
			$('#superUserForm').find('#username').focus();
			return;
		}).always(function(data){
			// do nothing
		});
}

/**
	resetSuperUserForm
**/
function resetSuperUserForm(){
	$('#superUserForm').find('#username').prop('disabled', false);
	$('#superUserForm').find('#username').val("");
	$('#superUserForm').find('#password').val("");
	$('#superUserForm').find('#userid').val("");
	$('#superUserForm').find('#saveLabel').html("User [NEW]");
	$('#superUserForm').find('#username').focus();	
}