/**
	loadUserView
**/
function loadUserView(){
	$('#contentDiv').load(
	"views/viewUser.html",
	function(response, status, xhr){
		if(status == "success"){
			initUploadUI();
			
			$('#accordionUserView').accordion({
				collapsible: false,
				autoHeight: false,
				heightStyle: 'content',
				clearStyle: true
			});
			
			$('#accordionUserView').accordion().children('.ui-accordion-header').hide();
			
			$('#divUserMenu a').click(
				function(event){
					var user_menu_index = $(this).data("user-menu-index");
					user_menu_index = parseInt(user_menu_index);
					if(user_menu_index == ($('#divUserMenu a').length - 1)){
						logOut();
					}else{
						$('#accordionUserView').accordion('activate', user_menu_index);
					}
					event.preventDefault();
				}
			);
			
			$('#divUserView').fadeIn(1000);
			
			$('#normalUserForm').find('#btnReset').click(
				function(event){
					resetUserForm();
					event.preventDefault();
				}
			);
			
			$('#normalUserForm').find('#btnSave').click(
				function(event){
					saveUserForm();
					event.preventDefault();
				}
			);
			
			$('#cmbUserProjects').change(
				function(){
					loadUserCoverage();
				}
			);
			
			$('#cmbAdminCoverageProjects').change(
				function(){
					loadAdminCoverageReports();
				}
			);
			
			resetUserForm();
			loadUserProjects();
		}else{
			var msg = "There is an error: ";
			alert(msg + xhr.status + " " + xhr.statusText);
		}
	});
}

//*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//*$$$$$$$$$$$$$$$$$$$$$$$ USER $$$$$$$$$$$$$$$$$$$$$$$
//*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

function initUploadUI(){
	$('#coverageFileUpload').uploadFile(
		url: __APP_COVERAGE_URL,
		multiple: false,
		allowedTypes: 'exec',
		uploadButtonClass: 'ajax-file-upload-green',
		returnType: 'json',
		dragDropStr: '',
		formData: {
			"userid" : __userSession.getUser().getUserid(),
			"type" : 'coverage'
		},
		dynamicFormData: function(){
			var data = {projectid: $('#cmbUserProjects').val()}
			return data;
		},
		onSuccess: function(files, data, xhr){
			if(data[0].hasOwnProperty('error') && data[0].error != ""){
				alert("Error: " + data[0].error);
				return;
			}else{
				var content = getUserCoverageRow(data[0]);
				if(data[0].type === "update"){
					$('#tableUserCoverageBody td').filter(function(){
						return $(this).text().toLowerCase() == data[0].coveragename;
					}).parent().replaceWith(content);
				}else{
					$('#tableUserCoverageBody').append(content);
				}
			}
		},
		onError: function(files, status, errMsg){
			alert("Error for: "+ JSON.stringify(files));
		}
	);
}

/**
	saveUserForm
**/
function saveUserForm(){
	if(!$.trim($('#normalUserForm').find('#password').val()).length()){
		alert("Warning: Password is required.");
		$('#normalUserForm').find('#password').focus();
		return;
	}
	
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_UPDATE_USER,
		sessionid: __userSession.getSessionId(),
		userid: __userSession().getUser().getUserid(),
		password: $.trim($('#normalUserForm').find('#password').val())
	}).done(
		function(responseData){
			data = $(responseData[0]);
			if(data.error != ""){
				alert("Error: " + data.error);
				$('#normalUserForm').find('#password').focus();
				return;
			}else{
				alert("Success: User data saved.");
				$('#normalUserForm').find('#password').focus();
			}
		}).fail(function(data){
			alert("Error: Unable to process 'saveUserForm' request");
			$('#normalUserForm').find('#password').focus();
			return;
		}).always(function(data){
			// do nothing
		});
}

/**
	resetUserForm
**/
function resetUserForm(){
	$('#normalUserForm').find('#username').val(
		__userSession().getUser().getUsername()
	);
	$('#normalUserForm').find('#password').val("");
	$('#normalUserForm').find('#password').focus();
}


//*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//*$$$$$$$$$$$$$$$$$$$$$$$ PROJECTS $$$$$$$$$$$$$$$$$$$$$$$
//*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

/**
	loadUserProjects
**/
function loadUserProjects(){
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_GET_PROJECT_LIST,
		sessionid: __userSession.getSessionId(),
		userid: __userSession().getUser().getUserid()		
	}).done(
		function(responseData){
			data = $(responseData);
			
			// 1st index is always 'error' object
			if(data[0].hasOwnProperty('error') && data[0].error != ""){
				alert("Error: " + data[0].error);
				return;
			}else{
				$.each($(data), function(idx, obj){
					if(idx === 0){
						return; // 1st element is error object
					}
					
					$('#cmbUserProjects').append(
						$("<option />").val(obj.projectid).text(obj.projectname);
					);
					
					$('#cmbAdminCoverageProjects').append(
						$("<option />").val(obj.projectid).text(obj.projectname);
					);
				});
				
				$('#cmbUserProjects').prop('selectedIndex', 0);
				$('#cmbAdminCoverageProjects').prop('selectedIndex', 0);
				
				loadUserCoverage();
				loadAdminCoverageReports();
			}
			$('#adminProjectForm').find('#projectname').focus();
		}).fail(function(data){
			alert("Error: Unable to process 'loadUserProjects' request");
			return;
		}).always(function(data){
			// finished
		});
}


//*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//*$$$$$$$$$$$$$$$$$$$$$$$ COVERAGE $$$$$$$$$$$$$$$$$$$$$$$
//*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

/**
	loadUserCoverage
**/
function loadUserCoverage(){
	$('#tableUserCoverageBody').find("tr").remove();
	
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_GET_COVERAGE_LIST,
		sessionid: __userSession.getSessionId(),
		userid: __userSession().getUser().getUserid(),
		projectid: $('#cmbUserProjects').val()		
	}).done(
		function(responseData){
			data = $(responseData);
			
			// 1st index is always 'error' object
			if(data[0].hasOwnProperty('error') && data[0].error != ""){
				alert("Error: " + data[0].error);
				return;
			}else{
				$.each($(data), function(idx, obj){
					if(idx === 0){
						return; // 1st element is error object
					}

					$('#tableUserCoverageBody').append(getUserCoverageRow(obj));
				});
			}
		}).fail(function(data){
			alert("Error: Unable to process 'loadUserCoverage' request");
			return;
		}).always(function(data){
			// finished
		});
}


/**
	getUserCoverageRow
**/
function getUserCoverageRow(obj){
	var content = "<tr>"
	+ "<td>" + obj.coverageproject + "</td>"
	+ "<td>" + obj.coveragename + "</td>"
	+ "<td>" + obj.coveragedate + "</td>"
	+ "</tr>";
	return content;
}
