/**
	loadAdminView
**/
function loadAdminView(){
	$('#contentDev').load(
		"views/viewAdmin.html",
		function(response, status, xhr){
			if(status == "success"){
				initProjectSourceUplaodUI();
				
				$('#accordionAdminView').accordion({
					collapsible: false,
					autoHeight: false;
					heightStyle: 'content',
					animate:false
				});
				
				$('#accordionAdminView').accordion().children('.ui-accordion-header').hide();
				
				$('#divAdminMenu a').click(
					function(event){
						var admin_menu_index = $(this).data('admin-menu-index');
						admin_menu_index = parseInt(admin_menu_index);
						i(admin_menu_index == ( $('divAdminMenu a').length - 1 )){
							logOut();
						}else{
							resetAdminUserForm();
							resetAdminAccountForm();
							resetAdminProjectForm();
							$('#accordionAdminView').accordion('activate', admin_menu_index);
						}	
						event.preventDefault();		
					}
				);
				
				$('#divAdminView').fadeIn(1000);
				$('#adminUserForm').find('#btnReset').click(
					function(event){
						resetAdminUserForm();
						event.preventDefault();	
					}
				);
				$('#adminUserForm').find('#btnSave').click(
					function(event){
						saveAdminUser('N');
						event.preventDefault();	
					}
				);
				$('#adminProjectForm').find('#btnProjectReset').click(
					function(event){
						resetAdminProjectForm();
						event.preventDefault();	
					}
				);
				$('#adminProjectForm').find('#btnProjectSave').click(
					function(event){
						saveAdminProject('N');
						event.preventDefault();	
					}
				);
				$('#adminAccountForm').find('#btnAdminAccountSave').click(
					function(event){
						saveAdminAccountForm();
						event.preventDefault();	
					}
				);
				$('#cmbAdminProjects').change(
					function(){
						loadAdminCoverage();	
					}
				);
				$('#cmbAdminCoverageProjects').change(
					function(){
						loadAdminCoverageReports();	
					}
				);
				$('#accordionAdminView').find('#btnGenerateCoverage').click(
					function(event){
						generateCoverageReport();
						event.preventDefault();	
					}
				);
				
				loadAdminProjects();
				loadUsers();
				loadProjects();
				resetAdminAccountForm();
			}else{
				var msg = "Sorry but there was an error: ";
				alert(msg + xhr.status + " " + xhr.statusText);
			}
		});
}

/**
	loadAdminProjects
**/
function loadAdminProjects(){
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_GET_PROJECT_LIST,
		sessionid: __userSession.getSessionId(),
		userid: __userSession().getUser().getUserid()		
	}).done(
		function(responseData){
			data = $(responseData);
			
			// 1st index is always 'error' object
			if(data[0].hasOwnProperty('error') && data[0].error != ""){
				alert("Error: " + data[0].error);
				return;
			}else{
				$.each($(data), function(idx, obj){
					if(idx === 0){
						return; // 1st element is error object
					}

					$('#cmbAdminProjects').appnd(
						$("<option />").val(obj.projectid).text(obj.projectname);
					);		
					
					$('#cmbAdminCoverageProjects').appnd(
						$("<option />").val(obj.projectid).text(obj.projectname);
					);
				});
				
				$('#cmbAdminProjects').prop('selectedIndex', 0);
				$('#cmbAdminCoverageProjects').prop('selectedIndex', 0);
				
				loadAdminCoverage();
				loadAdminCoverageReports();
			}
		}).fail(function(data){
			alert("Error: Unable to process 'loadAdminProjects' request");
			return;
		}).always(function(data){
			// finished
		});
}

/**
	loadAdminCoverage
**/
function loadAdminCoverage(){
	$('#tableAdminCoverageBody').find("tr").remove();
	$('#admincoveragename').val("");
	
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_GET_COVERAGE_LIST,
		sessionid: __userSession.getSessionId(),
		userid: __userSession().getUser().getUserid(),
		projectid: $('#cmbAdminProjects').val()		
	}).done(
		function(responseData){
			data = $(responseData);
			
			// 1st index is always 'error' object
			if(data[0].hasOwnProperty('error') && data[0].error != ""){
				alert("Error: " + data[0].error);
				return;
			}else{
				$.each($(data), function(idx, obj){
					if(idx === 0){
						return; // 1st element is error object
					}

					$('#tableAdminCoverageBody').append(getAdminCoverageRow(obj));
				});
			}
		}).fail(function(data){
			alert("Error: Unable to process 'loadAdminCoverage' request");
			return;
		}).always(function(data){
			// finished
		});
}

/**
	getAdminCoverageRow
**/
function getAdminCoverageRow(obj){
	var content = "<tr><td align='center'><input type='checkbox' value='"
	+ obj.coveragename + "'></td><td>" + obj.coverageproject + "</td>"
	+ "<td>" + obj.coveragename + "</td>"
	+ "<td>" + obj.coveragedate + "</td></tr>";
	return content;
}

/**
	loadAdminCoverageReports
**/
function loadAdminCoverageReports(){
	$('#loadAdminCoverageReportBody').find("tr").remove();
	
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_GET_REPORT_LIST,
		sessionid: __userSession.getSessionId(),
		userid: __userSession().getUser().getUserid(),
		projectid: $('#cmbAdminCoverageProjects').val()		
	}).done(
		function(responseData){
			data = $(responseData);
			
			// 1st index is always 'error' object
			if(data[0].hasOwnProperty('error') && data[0].error != ""){
				alert("Error: " + data[0].error);
				return;
			}else{
				$.each($(data), function(idx, obj){
					if(idx === 0){
						return; // 1st element is error object
					}

					$('#loadAdminCoverageReportBody').append(getAdminCoverageReportRow(obj));
				});
			}
		}).fail(function(data){
			alert("Error: Unable to process 'loadAdminCoverageReports' request");
			return;
		}).always(function(data){
			// finished
		});
}

/**
	getAdminCoverageReportRow
**/
function getAdminCoverageReportRow(obj){
	var content = "<tr><td>&nbsp;" + obj.reportname + "&nbsp;</td>"
	+ "<td>&nbsp;" + obj.reportdate + "&nbsp;</td>"
	+ "<td>&nbsp;<a href='" + obj.reportlink + "' target='_blank'>View</a>&nbsp;</td></tr>";
	return content;
}

/**
	generateCoverageReport
**/
function generateCoverageReport(){
	if(!$.trim($('#accordionAdminView').find('#admincoveragename').val()).length()){
		alert("Warning: Coverage name is required.");
		$('#accordionAdminView').find('#admincoveragename').focus();
		return;
	}
	
	if(!$.trim($('#accordionAdminView').find('#admincoveragename').val()).length() > 20){
		alert("Warning: Coverage name shoule be maximum 20 characters.");
		$('#accordionAdminView').find('#admincoveragename').focus();
		return;
	}
	
	var checkboxValues = [];
	$('#tableAdminCoverageBody input[type="checkbox"]:checked').each(
		function(index, elem){
			checkboxValues.push($(elem).val());
		}
	);
	if(checkboxValues.length == 0){
		alert("Warning: Please select coverage from coverage list.");
		$('#accordionAdminView').find('#admincoveragename').focus();
		return;
	}else{
		// alert(checkboxValues.join(','));
	}
	
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_ADMIN_SAVE_REPORT,
		sessionid: __userSession.getSessionId(),
		userid: __userSession().getUser().getUserid(),
		projectid: $('#cmbAdminCoverageProjects').val(),
		coveragename: $.trim($('#accordionAdminView').find('#admincoveragename').val())
	}).done(
		function(responseData){
			data = $(responseData[0]);
			if(data.error != ""){
				alert("Error: " + data.error);
				return;
			}else{
				alert("Success: Coverage report '" + data.reportname + "' genearted.");
			}
		}).fail(function(data){
			alert("Error: Unable to process 'generateCoverageReport' request");
			return;
		}).always(function(data){
			$('#tableAdminCoverageBody input[type="checkbox"]:checked').each(
				function(index, elem){
					this.checked = false;
				}
			);
			$('#accordionAdminView').find('#admincoveragename').focus();
		});
}

/**
	saveAdminAccountForm
**/
function saveAdminAccountForm(){
	if(!$.trim($('#adminAccountForm').find('#accountPassword').val()).length()){
		alert("Warning: Password is required.");
		$('#adminAccountForm').find('#accountPassword').focus();
		return;
	}
	
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_UPDATE_USER,
		sessionid: __userSession.getSessionId(),
		userid: __userSession().getUser().getUserid(),
		password: $.trim($('#adminAccountForm').find('#accountPassword').val())
	}).done(
		function(responseData){
			data = $(responseData);
			if(data.error != ""){
				alert("Error: " + data.error);
				$('#adminAccountForm').find('#accountPassword').focus();
				return;
			}else{
				alert("Success: Account data saved.");
				$('#adminAccountForm').find('#accountPassword').focus();
			}
		}).fail(function(data){
			alert("Error: Unable to process 'saveAdminAccountForm' request");
			$('#adminAccountForm').find('#accountPassword').focus();
			return;
		}).always(function(data){
			// do nothing
		});
}

/**
	resetAdminAccountForm
**/
function resetAdminAccountForm(){
	$('#adminAccountForm').find('#accountUserName').val(
		__userSession().getUser().getUsername()
	);
	$('#adminAccountForm').find('#accountPassword').val("");
	$('#adminAccountForm').find('#accountPassword').focus();
}

/**
	loadUsers
**/
function loadUsers(){
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_ADMIN_GET_SUBORDINATES,
		sessionid: __userSession.getSessionId(),
		userid: __userSession().getUser().getUserid()		
	}).done(
		function(responseData){
			data = $(responseData);
			
			// 1st index is always 'error' object
			if(data[0].hasOwnProperty('error') && data[0].error != ""){
				alert("Error: " + data[0].error);
				return;
			}else{
				updateUsersList(data);
			}
			$('#adminUserForm').find('#username').focus();
		}).fail(function(data){
			alert("Error: Unable to process 'loadUsers' request");
			return;
		}).always(function(data){
			// finished
		});
}

/**
	updateUsersList
**/
function updateUsersList(data){
	__dataArray = new Array();
	$.each($(data), function(idx, obj){
		if(idx === 0){
			return; // 1st element is error object
		}
		
		__dataArray[obj.userid] = obj;
		var content = getAdminUserRow(obj);
		$('#tableAdminUserBody').append(content);
	});
	
	initAdminTableClicks();
}	

/**
	initAdminTableClicks
**/
function initAdminTableClicks(){
	$('#tableAdminUserBody a').click(
		function(event){
			var userid = $(this).data("userid");
			var user = __dataArray[userid];
			if(!user){
				resetAdminUserForm();
			}else{
				$('#adminUserForm').find('#username').prop('disabled', true);
				$('#adminUserForm').find('#username').val(user.username);
				$('#adminUserForm').find('#password').val(user.password);
				$('#adminUserForm').find('#userid').val(user.userid);
				$('#adminUserForm').find('#saveLabel').html("User [Update]");
				$('#adminUserForm').find('#password').focus();
			}	
			event.preventDefault();	
		}
	);
	
	$('#tableAdminUserBody span').click(
		function(event){
			var userid = $(this).data("userid");
			var user = __dataArray[userid];
			
			var answer = confirm("Are you sure you want to delete user '"
			+ user.username + "'?");
			
			if(answer){
				$('#adminUserForm').find('#userid').val(user.userid);
				saveAdminUser('Y');
			}else{
				return false;
			}
			event.preventDefault();
		}
	);
}

/**
	getAdminUserRow
**/
function getAdminUserRow(obj){
	var content = "<tr><td><a href='#' data-user-id='" + obj.userid + "'>"
	+ obj.username + "</a></td>"
	+ "<td>" + obj.active + "</td>"
	+ "<td align='center'>"; 
	if(obj.active == true){
		content += "<span data-user-id='" + obj.userid + "' class='ui-icon ui-icon-circle-close'></span>";
	}
	content += "</td></tr>"
	return content;
}

/**
	saveAdminUser
**/
function saveAdminUser(isDelete){
	if(isDelete === "N"){
		if(!$.trim($('#adminUserForm').find('#username').val()).length){
			alert("Warning: Username is required.");
			$('#adminUserForm').find('#username').focus();
			return;
		}
		
		if(!$.trim($('#adminUserForm').find('#password').val()).length){
			alert("Warning: Password is required.");
			$('#adminUserForm').find('#password').focus();
			return;
		}
	}
	
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_ADMIN_CREATE_SUBORDINATE,
		sessionid: __userSession.getSessionId(),
		userid: $('#adminUserForm').find('#userid').val(),
		username:  (isDelete === "N") ? $.trim($('#adminUserForm').find('#username').val()) : "",
		password: (isDelete === "N") ? $.trim($('#adminUserForm').find('#password').val()) : ""
	}).done(
		function(responseData){
			data = responseData[0];
			if(data.error != ""){
				alert("Error: " + data.error);
				$('#adminUserForm').find('#username').focus();
				return;
			}else{
				delete __dataArray[data.userid];
				__dataArray[data.userid] = data;
				
				var content = getAdminUserRow(data);
				$('#adminTableUser td').filter(
					function(){
						return $(this).text().toLowerCase() == __dataArray[data.userid].username;
					}
				).parent().replaceWith(content);
				
				if(!$.trim($('#adminUserForm').find('#userid').val()).length){
					// new entry. add to user table
					var content = getAdminUserRow(data);
					$('#tableAdminUserBody').append(content);
					$('#adminUserForm').find('#userid').val(data.userid);
				}	
				
				if(isDelete === "Y"){
					resetAdminUserForm();
				}else{
					$('#adminUserForm').find('#username').prop('disabled', true);
					$('#adminUserForm').find('#userid').val(user.userid);
					$('#adminUserForm').find('#saveLabel').html("User [Update]");
					$('#adminUserForm').find('#password').focus();
				}
				
				initAdminTableClicks();
			}
		}).fail(function(data){
			alert("Error: Unable to process 'saveAdminUser' request");
			$('#adminUserForm').find('#username').focus();
			return;
		}).always(function(data){
			// do nothing
		});
}

/**
	resetAdminUserForm
**/
function resetAdminUserForm(){
	$('#adminUserForm').find('#username').prop('disabled', false);
	$('#adminUserForm').find('#username').val("");
	$('#adminUserForm').find('#password').val("");
	$('#adminUserForm').find('#userid').val("");
	$('#adminUserForm').find('#saveLabel').html("User [NEW]");
	$('#adminUserForm').find('#username').focus();	
}

/**
	loadProjects
**/
function loadProjects(){
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_GET_PROJECT_LIST,
		sessionid: __userSession.getSessionId(),
		userid: __userSession().getUser().getUserid()		
	}).done(
		function(responseData){
			data = $(responseData);
			
			// 1st index is always 'error' object
			if(data[0].hasOwnProperty('error') && data[0].error != ""){
				alert("Error: " + data[0].error);
				return;
			}else{
				updateAdminProjectsList(data);
			}
			$('#adminProjectForm').find('#projectname').focus();
		}).fail(function(data){
			alert("Error: Unable to process 'loadProjects' request");
			return;
		}).always(function(data){
			// finished
		});
}

/**
	updateAdminProjectsList
**/
function updateAdminProjectsList(data){
	__projectsArray = new Array();
	$.each($(data), function(idx, obj){
		if(idx === 0){
			return; // 1st element is error object
		}
		
		__projectsArray[obj.projectid] = obj;
		var content = getAdminProjectRow(obj);
		$('#tableAdminProjectBody').append(content);
	});
	initAdminProjectTableClicks();
}

/**
	initAdminProjectTableClicks
**/
function initAdminProjectTableClicks(){
	$('#tableAdminProjectBody a').click(
		function(event){
			var projectid = $(this).data("projectid");
			var project = __projectsArray[projectid];
			if(!project){
				resetAdminProjectForm();
			}else{
				$('#adminProjectForm').find('#projectname').prop('disabled', true);
				$('#adminProjectForm').find('#projectname').val(project.projectname);
				$('#adminProjectForm').find('#projectdesc').val(project.projectdesc);
				$('#adminProjectForm').find('#projectid').val(project.projectid);
				$('#adminProjectForm').find('#saveProjectLabel').html("Project [Update]");
				$('#adminProjectForm').find('#projectdesc').focus();
				
				$('#coverageSourceUploadLine').show();
				$('#coverageSourceUploadHolder').show();
			}	
			event.preventDefault();	
		}
	);
	
	$('#tableAdminProjectBody span').click(
		function(event){
			var projectid = $(this).data("projectid");
			var project = __projectsArray[projectid];
			
			var answer = confirm("Are you sure you want to delete project '"
			+ project.projectname + "'?");
			
			if(answer){
				$('#adminProjectForm').find('#projectid').val(project.projectid);
				saveAdminProject('Y');
			}else{
				return false;
			}
			event.preventDefault();
		}
	);
}

/**
	getAdminProjectRow
**/
function getAdminProjectRow(obj){
	var content = "<tr><td><a href='#' data-project-id='" + obj.projectid + "'>"
	+ obj.projectname + "</a></td>"
	+ "<td>" + obj.active + "</td>"
	+ "<td align='center'>"; 
	if(obj.active == true){
		content += "<span data-project-id='" + obj.projectid + "' class='ui-icon ui-icon-circle-close'></span>";
	}
	content += "</td></tr>"
	return content;
}

/**
	saveAdminProject
**/
function saveAdminProject(isDelete){
	if(isDelete === "N"){
		if(!$.trim($('#adminProjectForm').find('#projectname').val()).length){
			alert("Warning: Project name is required.");
			$('#adminProjectForm').find('#projectname').focus();
			return;
		}
		
		if(!$.trim($('#adminProjectForm').find('#projectdesc').val()).length){
			alert("Warning: Project description is required.");
			$('#adminProjectForm').find('#projectdesc').focus();
			return;
		}
	}
	
	var postResponse = $.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_ADMIN_SAVE_PROJECT,
		sessionid: __userSession.getSessionId(),
		userid: __userSession().getUser().getUserid(),
		projectid: $.trim($('#adminProjectForm').find('#projectid').val()),
		projectname:  (isDelete === "N") ? $.trim($('#adminProjectForm').find('#projectname').val()) : "",
		projectdesc: (isDelete === "N") ? $.trim($('#adminProjectForm').find('#projectdesc').val()) : ""
	}).done(
		function(responseData){
			data = responseData[0];
			if(data.error != ""){
				alert("Error: " + data.error);
				$('#adminProjectForm').find('#projectname').focus();
				return;
			}else{
				delete __projectsArray[data.projectid];
				__projectsArray[data.projectid] = data;
				
				var content = getAdminProjectRow(data);
				$('#tableAdminProjectBody td').filter(
					function(){
						return $(this).text().toLowerCase() == __projectsArray[data.projectid].projectname;
					}
				).parent().replaceWith(content);
				
				if(!$.trim($('#adminProjectForm').find('#projectid').val()).length){
					// new entry. add to user table
					var content = getAdminProjectRow(data);
					$('#tableAdminProjectBody').append(content);
					$('#adminProjectForm').find('#projectid').val(data.projectid);
				}	
				
				if(isDelete === "Y"){
					resetAdminProjectForm();
				}else{
					$('#adminProjectForm').find('#projectname').prop('disabled', true);
					$('#adminProjectForm').find('#projectid').val(user.userid);
					$('#adminProjectForm').find('#saveProjectLabel').html("Project [Update]");
					$('#adminProjectForm').find('#projectdesc').focus();
				}
				
				initAdminProjectTableClicks();
			}
		}).fail(function(data){
			alert("Error: Unable to process 'saveAdminProject' request");
			$('#adminProjectForm').find('#projectname').focus();
			return;
		}).always(function(data){
			// do nothing
		});
}

/**
	resetAdminProjectForm
**/
function resetAdminProjectForm(){
	$('#adminProjectForm').find('#projectname').prop('disabled', false);
	$('#adminProjectForm').find('#projectname').val("");
	$('#adminProjectForm').find('#projectdesc').val("");
	$('#adminProjectForm').find('#projectid').val("");
	$('#adminProjectForm').find('#saveProjectLabel').html("Project [NEW]");
	$('#adminProjectForm').find('#projectname').focus();	
	
	$('#coverageSourceUploadLine').hide();
	$('#coverageSourceUploadHolder').hide();	
}

/**
	initProjectSourceUploadUI
**/
function initProjectSourceUploadUI(){
	$('#coverageSourceUpload').uploadFile({
		url: __APP_COVERAGE_URL,
		multiple: false,
		allowedTypes: "jar",
		uploadButtonClass: "ajax-file-upload-green",
		returnType:"json",
		dragDropStr:'',
		formData:{
			"userid" : __userSession().getUser().getUserid(),
			"type": 'source'
		},
		dynamicFormData:{
			var data : {projectid: $('#adminProjectForm').find('#projectid').val()}
			return data;
		},
		onSubmit:function(){
			$.blockUI();
		},
		onSuccess: function(files, data, xhr){
			$.unblockUI();
			if(data[0].hasOwnProperty('error') && data[0].error != ""){
				alert("Error: " + data[0].error);
				return;
			}else{
				// success
			}
		},
		onError: function(files, status, errMsg){
			$.unblockUI();
			alert("Error for: " + JSON.stringify(files));
		}	
	});	
}
