$.themes.setDefaults({
	cookieExpiry: 7,
	themeBase: 'lib/jqthemes/themes/',
	icons: 'lib/jqthemes/themes.gif',
	previews: 'lib/jqthemes/themes-preview.gif',
	showPreview: false,
	defaultTheme: 'uilightness'
});

$(function(){
	$('#uiThemes').themes(
		{themes: ['uilightness', 'start']}
	);	
	$('#date').datepicker();
});

/**
*	Application Functions
**/

// Singleton User Session Class
var __userSession = new function(){
	this.sessionId;
	this.user = new User();
	
	this.setSessionId = function(_sessionId){
		this.sessionId = _sessionId;
	};
	this.getSessionId = function(){
		return this.sessionId;
	};
	
	this.getUser = function(){
		return this.user;
	};
};

// User Class
function User(){
	this.username;
	this.userid;
	this.type;

	this.setUsername = function(_username){
		this.username = _username;
	};	
	this.getUsername = function(){
		return this.username;
	};
	
	this.setUserid = function(_userid){
		this.userid = _userid;
	};	
	this.getUserid = function(){
		return this.userid;
	};
	
	this.setType = function(_type){
		this.type = _type;
	};	
	this.getType = function(){
		return this.type;
	};
}

// CONSTANTS
const __APP_SERVICE_URL = "/coverageserver/appService";
const __APP_COVERAGE_URL = "/coverageserver/uploadCoverage";

const QUERY_TYPE_VALIDATE_LOGIN = "qtvl";
const QUERY_TYPE_UPDATE_USER = "qtus";
const QUERY_TYPE_GET_PROJECT_LIST = "qtgpl";
const QUERY_TYPE_GET_COVERAGE_LIST = "qtgcl";
const QUERY_TYPE_GET_REPORT_LIST = "qtgrl";

const QUERY_TYPE_SUPER_CREATE_ADMIN_USER = "qtscau";
const QUERY_TYPE_SUPER_GET_ADMIN_USER_LIST = "qtsgaul";

const QUERY_TYPE_ADMIN_GET_SUBORDINATES = "qtags";
const QUERY_TYPE_ADMIN_CREATE_SUBORDINATE = "qtacs";
const QUERY_TYPE_ADMIN_SAVE_PROJECT = "qtasp";
const QUERY_TYPE_ADMIN_SAVE_REPORT = "qtasr";
const QUERY_TYPE_USER_LOGOUT = "qtlo";

const USER_TYPE_SUPER_ADMIN = "SUPER";
const USER_TYPE_ADMIN = "ADMIN";
const USER_TYPE_REGULAR = "REGULAR";

// Global arrays
var __dataArray = new Array();
var __projectsArray = new Array();

// Boilerplate code

$(document).ready(function(){
	'use strict';
	$('body').addClass('js');
	$("button, input:submit, input:button").button();
	
	loadLoginView();
});

/**
	loadLoginView
**/
function loadLoginView(){
	$('#contentDiv').load(
	"views/viewLogin.html",
	function(response, status, xhr){
		if(status == "success"){
			$('.login-form span').addClass('checked').children('input').attr('checked', true);
			
			$('.login-form span').on('click', function(){
				if($(this).children('input').attr('checked')){
					$(this).children('input').attr('checked', false);
					$(this).removeClass('checked');
				}else{
					$(this).children('input').attr('checked', true);
					$(this).addClass('checked');
				}
			});
			
			$('#login-form').submit(function(e){
				return false;
			});
			
			$('#btnLogin').click(function(event){
				event.preventDefault();
				validateLogin();
			});
			$('#divView').fadeIn(1000);
			
			$('#username').val($.cookie('wishcoder.coverage.username'));
			$('#username').focus();
		}else{
			var msg = "There is an error: ";
			jAlert(msg + xhr.status + " " + xhr.statusText, "Code Coverage - Error");
		};
	});
}

/**
	validateLogin
**/	
function validateLogin(){
	if(!$.trim($('#username').val()).length){
		jAlert("Username is required.", "Code Coverage - Warning");
		$('#username').focus();
		return;
	}
	
	if(!$.trim($('#password').val()).length){
		jAlert("Password is required.", "Code Coverage - Warning");
		$('#password').focus();
		return;
	}
	
	$.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_VALIDATE_LOGIN,
		sessionid: '',
		username: $.trim($('#username').val()),
		password: $.trim($('#password').val())
	}).done(
		function(responseData){
			data = $(responseData);
			
			if(data[0].error != ""){
				jAlert(data[0].error, "Code Coverage - Error");
				$('#username').focus();
				return;
			}else{
				__userSession.setSessionId(data[0].sessionid);
				__userSession.getUser().setUserid(data[0].userid);
				__userSession.getUser().setUsername(data[0].username);
				__userSession.getUser().setType(data[0].usertype);
				
				if($('#chkRemember').is(':checked')){
					$.cookie('wishcoder.coverage.username', $.trim($('#username').val())
					, {expires:365});
				}else{
					$.cookie('wishcoder.coverage.username', "");
				}
				
				$('#divView').fadeOut(1000);
				
				if(__userSession.getUser().getType() === USER_TYPE_SUPER_ADMIN){
					loadSuperView();
				} else if(__userSession.getUser().getType() === USER_TYPE_ADMIN){
					loadAdminView();
				} else if(__userSession.getUser().getType() === USER_TYPE_REGULAR){
					loadUserView();
				}
			}
		}).fail(function(data){
			jAlert("Unable to process 'validateLogin' request", "Code Coverage - Error");
			$('#username').focus();
			return;
		}).always(function(data){
			return;
		});
}


/**
sendLogout
**/	
function sendLogout(){

$.post(__APP_SERVICE_URL, {
	qtype: QUERY_TYPE_USER_LOGOUT,
	sessionid: __userSession.getSessionId()
}).done(
	function(responseData){
		data = $(responseData);
		
		if(data[0].error != ""){
			jAlert(data[0].error, "Code Coverage - Error");
			return;
		}else{
			
			__userSession.setSessionId("");
			__userSession.getUser().setUserid("");
			__userSession.getUser().setUsername("");
			__userSession.getUser().setType("");

			$('#divView').fadeOut(1000);
			
			loadLoginView();
			
		}
	}).fail(function(data){
		jAlert("Unable to process 'logout' request", "Code Coverage - Error");
		return;
	}).always(function(data){
		return;
	});
}

/**
	logOut
**/
function logOut(){
	sendLogout();
};