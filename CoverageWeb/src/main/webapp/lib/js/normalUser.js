/**
	loadUserView
**/
function loadUserView(){
	$('#contentDiv').load(
	"views/viewUser.html",
	function(response, status, xhr){
		if(status == "success"){
			initUploadUI();
			
			$('#accordionUserView').accordion({
				collapsible: false,
				autoHeight: false,
				heightStyle: 'content',
				clearStyle: true
			});
			
			$('#accordionUserView').accordion().children('.ui-accordion-header').hide();
			
			$('#divUserMenu a').click(
				function(event){
					var user_menu_index = $(this).data("user-menu-index");
					user_menu_index = parseInt(user_menu_index);
					if(user_menu_index == ($('#divUserMenu a').length - 1)){
						logOut();
					}else{
						$('#accordionUserView').accordion('activate', user_menu_index);
					}
					event.preventDefault();
				}
			);
			
			$('#divUserView').fadeIn(1000);
			
			$('#normalUserForm').find('#btnReset').click(
				function(event){
					resetUserForm();
					event.preventDefault();
				}
			);
			
			$('#normalUserForm').find('#btnSave').click(
				function(event){
					saveUserForm();
					event.preventDefault();
				}
			);
			
			$('#cmbUserProjects').change(
				function(){
					loadUserCoverage();
				}
			);
			
			$('#cmbAdminCoverageProjects').change(
				function(){
					loadAdminCoverageReports();
				}
			);
			
			resetUserForm();
			loadUserProjects();
		}else{
			var msg = "There is an error: ";
			jAlert(msg + xhr.status + " " + xhr.statusText, "Code Coverage - Error");
		}
	});
}

//*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//*$$$$$$$$$$$$$$$$$$$$$$$ USER $$$$$$$$$$$$$$$$$$$$$$$
//*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

function initUploadUI(){
	$('#coverageFileUpload').uploadFile({
		url: __APP_COVERAGE_URL,
		multiple: false,
		allowedTypes: "exec",
		uploadButtonClass: "ajax-file-upload-green",
		returnType:"json",
		dragDropStr:'',
		formData:{
			"sessionid" : __userSession.getSessionId(),
			"userid" : __userSession.getUser().getUserid(),
			"type": 'coverage'
		},
		dynamicFormData:function(){
			var data = {projectid: $('#cmbUserProjects').val()};
			return data;
		},
		onSubmit:function(){
			$.blockUI();
		},
		onSuccess: function(files, data, xhr){
			$.unblockUI();
			if(data[0].hasOwnProperty('error') && data[0].error != ""){
				jAlert(data[0].error, "Code Coverage - Error");
				return;
			}else{
				var content = getUserCoverageRow(data[0]);
				if(data[0].type === "update"){
					$('#tableUserCoverageBody td').filter(function(){
						return $(this).text().toLowerCase() == data[0].coveragename;
					}).parent().replaceWith(content);
				}else{
					$('#tableUserCoverageBody').append(content);
				}
			}
		},
		onError: function(files, status, errMsg){
			$.unblockUI();
			jAlert("Error for: "+ JSON.stringify(files), "Code Coverage - Error");
		}	
	});	
}

/**
	saveUserForm
**/
function saveUserForm(){
	if(!$.trim($('#normalUserForm').find('#password').val()).length){
		jAlert("Password is required.", "Code Coverage - Warning");
		$('#normalUserForm').find('#password').focus();
		return;
	}
	
	$.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_UPDATE_USER,
		sessionid: __userSession.getSessionId(),
		userid: __userSession.getUser().getUserid(),
		password: $.trim($('#normalUserForm').find('#password').val())
	}).done(
		function(responseData){
			data = $(responseData[0]);
			if(data.error != ""){
				jAlert(data.error, "Code Coverage - Error");
				$('#normalUserForm').find('#password').focus();
				return;
			}else{
				jAlert("User data saved.", "Code Coverage - Success");
				$('#normalUserForm').find('#password').focus();
			}
		}).fail(function(data){
			jAlert("Unable to process 'saveUserForm' request", "Code Coverage - Error");
			$('#normalUserForm').find('#password').focus();
			return;
		}).always(function(data){
			// do nothing
		});
}

/**
	resetUserForm
**/
function resetUserForm(){
	$('#normalUserForm').find('#username').val(
		__userSession.getUser().getUsername()
	);
	$('#normalUserForm').find('#password').val("");
	$('#normalUserForm').find('#password').focus();
}


//*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//*$$$$$$$$$$$$$$$$$$$$$$$ PROJECTS $$$$$$$$$$$$$$$$$$$$$$$
//*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

/**
	loadUserProjects
**/
function loadUserProjects(){
	$.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_GET_PROJECT_LIST,
		sessionid: __userSession.getSessionId(),
		userid: __userSession.getUser().getUserid()		
	}).done(
		function(responseData){
			data = $(responseData);
			
			// 1st index is always 'error' object
			if(data[0].hasOwnProperty('error') && data[0].error != ""){
				jAlert(data[0].error, "Code Coverage - Error");
				return;
			}else{
				$.each($(data), function(idx, obj){
					if(idx === 0){
						return; // 1st element is error object
					}
					
					$('#cmbUserProjects').append(
						$("<option />").val(obj.projectid).text(obj.projectname)
					);
					
					$('#cmbAdminCoverageProjects').append(
						$("<option />").val(obj.projectid).text(obj.projectname)
					);
				});
				
				$('#cmbUserProjects').prop('selectedIndex', 0);
				$('#cmbAdminCoverageProjects').prop('selectedIndex', 0);
				
				loadUserCoverage();
				loadAdminCoverageReports();
			}
			$('#adminProjectForm').find('#projectname').focus();
		}).fail(function(data){
			jAlert("Unable to process 'loadUserProjects' request", "Code Coverage - Error");
			return;
		}).always(function(data){
			return;
		});
}


//*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//*$$$$$$$$$$$$$$$$$$$$$$$ COVERAGE $$$$$$$$$$$$$$$$$$$$$$$
//*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

/**
	loadUserCoverage
**/
function loadUserCoverage(){
	if($('#cmbUserProjects').val() == null){
		jAlert("Please select project.", "Code Coverage - Warning");
		$('#cmbUserProjects').focus();
		return;
	}
	
	$('#tableUserCoverageBody').find("tr").remove();
	
	$.post(__APP_SERVICE_URL, {
		qtype: QUERY_TYPE_GET_COVERAGE_LIST,
		sessionid: __userSession.getSessionId(),
		userid: __userSession.getUser().getUserid(),
		projectid: $('#cmbUserProjects').val()		
	}).done(
			function(responseData){
				data = $(responseData);
				
				// 1st index is always 'error' object
				if(data[0].hasOwnProperty('error') && data[0].error != ""){
					jAlert(data[0].error, "Code Coverage - Error");
					return;
				}else{
					$.each($(data), function(idx, obj){
						if(idx === 0){
							return; // 1st element is error object
						}

						$('#tableUserCoverageBody').append(getUserCoverageRow(obj));
					});
				}
	}).fail(function(data){
		jAlert("Unable to process 'loadUserCoverage' request", "Code Coverage - Error");
		return;
	}).always(function(data){
		return;
	});
}


/**
	getUserCoverageRow
**/
function getUserCoverageRow(obj){
	var content = "<tr>"
	+ "<td>" + obj.coverageproject + "</td>"
	+ "<td>" + obj.coveragename + "</td>"
	+ "<td>" + obj.coveragedate + "</td>"
	+ "</tr>";
	return content;
}
