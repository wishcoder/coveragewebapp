REM @ECHO OFF
set CLASSPATH=.
cls

java -agentlib:jdwp=transport=dt_socket,address=localhost:9009,server=y,suspend=y -Dserverport=8080 -Dadminpwd=admin007 -cp "WEB-INF/lib/*" us.wishcoder.coverage.server.CoverageServerMain OPTIONS=ALL